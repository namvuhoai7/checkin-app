import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React, { useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();
import MyTabs from './SideFunction/Navigation'
import LoginScreen from './screens/LoginScreen';
import SignUp from './screens/SignUp';
import PhoneVerify from './screens/PhoneVerify';
export default function App() {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen options={{ headerShown: false }} name="Login" component={LoginScreen} />
          <Stack.Screen options={{ headerShown: false }} name="SignUp" component={SignUp} />
          <Stack.Screen options={{ headerShown: false }} name="Phone" component={PhoneVerify} />
          <Stack.Screen options={{ headerShown: false }} name="Tabs" component={MyTabs} />
        </Stack.Navigator>
      </NavigationContainer>
    </>


  );
}
