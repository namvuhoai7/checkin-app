import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { initializeApp } from 'firebase/app';
import { getAuth } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCEWdo056OF2znuDynrSwXjQi9Q69m8kgI",
    authDomain: "igneous-mason-355913.firebaseapp.com",
    databaseURL: "https://igneous-mason-355913-default-rtdb.firebaseio.com",
    projectId: "igneous-mason-355913",
    storageBucket: "igneous-mason-355913.appspot.com",
    messagingSenderId: "420291539036",
    appId: "1:420291539036:web:213303452e4edb9759b699"
};

export const app = firebase.initializeApp(firebaseConfig)
export default firebaseConfig
export const auth = getAuth(app);

