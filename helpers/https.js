import { database } from "@firebase/database";

async function $dbGet(ref) {
  return new Promise((resolve) => {
    return database
      .ref(`ALLXONE/${ref}`)
      .get()
      .then((snapshot) => {
        if (snapshot.exists()) {
          return resolve(snapshot.val());
        } else {
          console.log("No data available");
          return resolve(false);
        }
      })
      .catch((error) => {
        console.error(error);
        return resolve(false);
      });
  });
}

async function $dbWatcher(ref, func) {
  database.ref(`ALLXONE/${ref}`).on("value", (snapshot) => {
    var val = snapshot.val();
    if (!func) return;
    func(val || null);
  });
}

async function $dbSet(ref, data) {
  return new Promise((resolve) => {
    return database.ref(`ALLXONE/${ref}`).set(data, (err) => {
      if (err) {
        alert(err.message);
        return resolve(false);
      }
      return resolve(true);
    });
  });
}

async function $dbRemove(ref) {
  return new Promise((resolve) => {
    database
      .ref(`ALLXONE/${ref}`)
      .remove()
      .then(() => {
        return resolve(true);
      });
  });
}

export { $dbGet, $dbWatcher, $dbSet, $dbRemove };
