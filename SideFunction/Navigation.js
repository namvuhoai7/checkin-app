import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeScreen from '../screens/HomeScreen';
import EmployeeProfile from '../screens/EmployeeProfile';
import SignOut from '../screens/SignOut';
const Tab = createMaterialBottomTabNavigator();

function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="login"
            activeColor="white"
            barStyle={{ backgroundColor: '#5f9ea0' }}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Profile"
                component={EmployeeProfile}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="account-wrench" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="SignOut"
                component={SignOut}
                options={{
                    tabBarLabel: 'SignOut',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="application-export" color={color} size={26} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

export default MyTabs;