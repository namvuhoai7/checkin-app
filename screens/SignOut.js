import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/core";
import { auth, app } from "../config";

const SignOut = () => {
  const navigation = useNavigation();

  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => {
        alert("Goodbye enjoy the rest of your day!");
        navigation.replace("Login");
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
      });
  };

  return (
    <>
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#5f9ea0",
          height: "19%",
        }}
      >
        <View>
          <TouchableOpacity
            style={{ flex: 1, marginTop: 10, flexDirection: "row" }}
          >
            <Image
              style={styles.tinyLogo3}
              source={require("../assets/front-avt.jpg")}
            ></Image>
            <Text
              style={{
                marginTop: 70,
                fontSize: 17,
                color: "white",
                fontWeight: "bold",
                paddingLeft: "2%",
              }}
            >
              Do you want to log out now?
            </Text>
          </TouchableOpacity>
        </View>
      </View>

      <View>
        <Image
          style={styles.tinyLogo}
          source={require("../assets/front-avt.jpg")}
        ></Image>
      </View>
      <View style={styles.button2}>
        <TouchableOpacity onPress={handleSignOut}>
          <Text style={styles.buttonText2}>Sign out</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default SignOut;

const styles = StyleSheet.create({
  button2: {
    backgroundColor: "orange",
    alignSelf: "center",
    width: "50%",
    marginTop: "-15%",
    height: "8%",
    borderRadius: 10,
    color: "brown",
  },
  buttonText2: {
    alignSelf: "center",
    paddingTop: "10%",
    color: "brown",
    fontWeight: "700",
    fontSize: 16,
  },
  tinyLogo3: {
    marginTop: "11.5%",
    marginLeft: "5.5%",
    borderRadius: 50,
    width: "20.9%",
    height: "50%",
  },
  tinyLogo: {
    alignSelf: "center",
    marginTop: "30%",
    width: "50%",
    height: "40%",
  },
});
