import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Pressable,
  Modal,
  KeyboardAvoidingView,
} from "react-native";
import React, { useEffect, useState } from "react";
import { auth } from "../config";
const picture = require("../assets/check.png");
import { useNavigation } from "@react-navigation/core";
import { getDatabase, ref, child, update, get } from "firebase/database";
import * as Location from "expo-location";
import { getDistance } from "geolib";

const HomeScreen = () => {
  //call some variables
  const db = getDatabase();
  const dbRef = ref(getDatabase());
  const userUid = auth.currentUser?.uid;
  const today = new Date();
  const updates = {};
  const navigation = useNavigation();
  const todayDate = today.getDate();
  const todayMonth = today.getMonth() + 1;
  const todayHours = today.getHours();
  const todayMinutes = today.getMinutes();
  const todaySeconds = today.getSeconds();
  const todayYears = today.getFullYear();

  //time
  const [hours, setHours] = useState("00");
  const [minutes, setMinutes] = useState("00");
  const [seconds, setSeconds] = useState("00");
  const [hours2, setHours2] = useState("00");
  const [minutes2, setMinutes2] = useState("00");
  const [seconds2, setSeconds2] = useState("00");

  // Check in out modify
  const [buttonModify, setButtonModify] = useState("Check in now");
  const [buttonCheckIn, setButtonCheckIn] = useState("#5f9ea0");
  const [textModify, setTextModify] = useState("white");
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState("");

  const [checkIn, setCheckIn] = useState("");
  const [checkOut, setCheckOut] = useState("");
  const [checkInImage, setCheckInImage] = useState("");
  const [checkOutImage, setCheckOutImage] = useState("");

  // Gps state
  const [latitudeCheck, setLatitudeCheck] = useState("");
  const [longitudeCheck, setLongitudeCheck] = useState("");
  const [distanceCheck, setDistanceCheck] = useState("");

  // update information
  const postData = {
    Date: todayDate + "/" + todayMonth + "/" + todayYears,
    Start: todayHours + ":" + todayMinutes + ":" + todaySeconds,
  };

  const postData2 = {
    Date: todayDate + "/" + todayMonth + "/" + todayYears,
    End: todayHours + ":" + todayMinutes + ":" + todaySeconds,
  };
  // get data from Firebase real-time database
  useEffect(() => {
    getUserInfo();
    getUserDate();
    getPictureModify();
  });
  useEffect(() => {
    getPermissions();
    getGpsLocation();
  }, [name, distanceCheck]);

  // get data from Firebase
  const getUserDate = () => {
    // Get information from firebase to check in
    get(
      child(
        dbRef,
        `/ALLXONE/${userUid}/CheckIn/${todayDate}-${todayMonth}/Start`
      )
    ).then((snapshot) => {
      if (snapshot.exists()) {
        const array = Object.values(snapshot.val());
        setCheckIn(array[0]);
        let vTime = array[1];
        vTime = vTime.split(":");
        setHours(vTime[0]);
        setMinutes(vTime[1]);
        setSeconds(vTime[2]);
      }
    });
    get(
      child(dbRef, `/ALLXONE/${userUid}/CheckIn/${todayDate}-${todayMonth}/End`)
    ).then((snapshot) => {
      if (snapshot.exists()) {
        const array = Object.values(snapshot.val());
        setCheckOut(array[0]);
        let vTime = array[1];
        vTime = vTime.split(":");
        setHours2(vTime[0]);
        setMinutes2(vTime[1]);
        setSeconds2(vTime[2]);
      }
    });
  };

  //picture modify
  const getPictureModify = () => {
    if (checkIn !== "") {
      setCheckInImage(picture);
      setButtonModify("Check out now");
      setButtonCheckIn("orange");
      setTextModify("#a0522d");
    }
    if (checkIn !== "" && checkOut !== "") {
      setCheckOutImage(picture);
      setButtonModify("Check in now");
      setButtonCheckIn("#5f9ea0");
      setTextModify("white");
    }
  };
  // get user information
  const getUserInfo = () => {
    get(child(dbRef, `ALLXONE/${userUid}/Information`))
      .then((snapshot) => {
        if (snapshot.exists()) {
          const array = Object.values(snapshot.val());
          setName(array[2]);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // get map permissions
  const getPermissions = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      setErrorMsg("Permission to access location was denied");
      return;
    }
  };

  // get map Gps
  const getGpsLocation = async () => {
    return new Promise(async (resolve) => {
      let location = await Location.getCurrentPositionAsync({
        accuracy: Location.Accuracy.Balanced,
      });
      setLatitudeCheck(location.coords.latitude);
      setLongitudeCheck(location.coords.longitude);
      getDistanceCalculation();
      return resolve(true);
    });
  };

  // get distance calculation
  const getDistanceCalculation = () => {
    return new Promise(async (resolve) => {
      var dis = getDistance(
        { latitude: 10.787958166366703, longitude: 106.6782129961472 },
        { latitude: latitudeCheck, longitude: longitudeCheck }
      );
      setDistanceCheck(dis);
      return resolve(true);
    });
  };

  // Check in function
  const onCheckInPress = () => {
    // distance is greater then 15m function alert only
    if (
      distanceCheck > 10000 ||
      distanceCheck == "" ||
      distanceCheck == undefined
    ) {
      getGpsLocation();
      alert("Please wait, loading your GPS!");
    } else if (distanceCheck > 15 || distanceCheck == "") {
      getGpsLocation();
      // when aver distance is lower than 15m then function works
      alert(
        "Your are curently out of check in range: " +
          `${distanceCheck}` +
          " meters"
      );

      setModalVisible(!modalVisible);
    } else if (distanceCheck < 15) {
      if (checkOutImage !== "") {
        alert("You are already checked out!");
        setModalVisible(!modalVisible);
      } else if (checkInImage === "" && checkOutImage === "") {
        // start
        if (
          (today.getHours() >= 8 && today.getHours() <= 9) ||
          (today.getHours() >= 12 && today.getHours() <= 13)
        ) {
          // update data to firebase
          updates[
            "/ALLXONE/" +
              userUid +
              "/CheckIn" +
              "/" +
              todayDate +
              "-" +
              todayMonth +
              "/Start"
          ] = postData;
          return update(ref(db), updates)
            .then(() => {
              setCheckInImage(picture);
              setModalVisible(!modalVisible);
              setButtonModify("Check out now");
              setButtonCheckIn("orange");
              setHours(today.getHours());
              setMinutes(today.getMinutes());
              setSeconds(today.getSeconds());
            })
            .catch((error) => {
              alert(error);
            });
        } else {
          alert("Out of the check in time please come back later");
          setModalVisible(!modalVisible);
        }
      } else if (checkInImage !== "" && checkOutImage === "") {
        if (
          (today.getHours() <= 19 && today.getHours() >= 17) ||
          (today.getHours() < 12 && today.getHours() > 11)
        ) {
          // update data to firebase
          updates[
            "/ALLXONE/" +
              userUid +
              "/CheckIn" +
              "/" +
              todayDate +
              "-" +
              todayMonth +
              "/End"
          ] = postData2;
          return update(ref(db), updates)
            .then(() => {
              setCheckOutImage(picture);
              setModalVisible(!modalVisible);
              setButtonModify("Check in now");
              setButtonCheckIn("#5f9ea0");
              setHours2(today.getHours());
              setMinutes2(today.getMinutes());
              setSeconds2(today.getSeconds());
            })
            .catch((error) => {
              alert(error);
            });
        } else {
          alert("Out of the check out time please come back later");
          setModalVisible(!modalVisible);
        }
      }
    }
  };

  // redirect to Profile page by clicking on Profile avatar
  const handleProfile = () => {
    navigation.navigate("Profile");
  };
  return (
    <>
      <KeyboardAvoidingView style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "#5f9ea0",
            height: "19%",
          }}
        >
          <View>
            <TouchableOpacity
              style={{ flex: 1, marginTop: 10, flexDirection: "row" }}
              onPress={handleProfile}
            >
              <Image
                style={styles.tinyLogo3}
                source={require("../assets/front-avt.jpg")}
              ></Image>
              <Text
                style={{
                  marginTop: 70,
                  fontSize: 17,
                  color: "white",
                  fontWeight: "bold",
                  paddingLeft: "2%",
                }}
              >
                Hello, Welcome back!
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.containerWidget}>
          <View style={styles.top}>
            <View>
              <Image
                style={{
                  width: 380,
                  height: 110,
                  resizeMode: "stretch",
                  alignSelf: "center",
                }}
                source={require("../assets/logo.png")}
              ></Image>
            </View>
          </View>
          <View style={styles.middle}>
            <View>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 20,
                  marginBottom: "5%",
                }}
              >
                Hello welcome back {name}!
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "#fff8dc",
                alignSelf: "center",
                textAlign: "center",
                borderWidth: 5,
              }}
            >
              <View style={{ width: "40%", borderRightWidth: 2.5 }}>
                <Text
                  style={{
                    backgroundColor: "grey",
                    color: "white",
                    textAlign: "center",
                    paddingTop: "10%",
                    fontSize: 20,
                    paddingBottom: "10%",
                  }}
                >
                  Start
                </Text>
                <View
                  style={{ borderTopWidth: 2.5, borderTopColor: "black" }}
                ></View>
                <Image
                  style={styles.tinyCheck}
                  source={Number(checkInImage)}
                ></Image>
                <View
                  style={{
                    borderTopWidth: 3,
                    marginTop: "10%",
                    marginBottom: "10%",
                    height: "20%",
                  }}
                >
                  <Text
                    style={{ textAlign: "center", paddingTop: 5, fontSize: 18 }}
                  >
                    {hours} : {minutes} : {seconds}
                  </Text>
                </View>
              </View>

              <View style={{ width: "40%", borderLeftWidth: 2.5 }}>
                <Text
                  style={{
                    backgroundColor: "grey",
                    color: "white",
                    textAlign: "center",
                    paddingTop: "10%",
                    fontSize: 20,
                    paddingBottom: "10%",
                  }}
                >
                  End
                </Text>
                <View
                  style={{ borderTopWidth: 2.5, borderTopColor: "black" }}
                ></View>
                <Image
                  style={styles.tinyCheck}
                  source={Number(checkOutImage)}
                ></Image>
                <View
                  style={{
                    borderTopWidth: 3,
                    marginTop: "10%",
                    marginBottom: "10%",
                    height: "20%",
                  }}
                >
                  <Text
                    style={{ textAlign: "center", paddingTop: 5, fontSize: 18 }}
                  >
                    {hours2} : {minutes2} : {seconds2}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{ marginTop: "10%" }}></View>
          </View>
          <View style={styles.bottom}>
            <View style={styles.centeredView}>
              <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                  Alert.alert("Modal has been closed.");
                  setModalVisible(!modalVisible);
                }}
              >
                <View style={styles.centeredView}>
                  <View style={styles.modalView}>
                    <Text style={styles.modalText}>
                      Do you really want to check in now ?
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <Pressable
                        style={[styles.acceptBut]}
                        onPress={() => onCheckInPress()}
                      >
                        <Text style={styles.textStyle}>Check!</Text>
                      </Pressable>
                      <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setModalVisible(!modalVisible)}
                      >
                        <Text style={styles.textStyle}>Go back</Text>
                      </Pressable>
                    </View>
                  </View>
                </View>
              </Modal>
            </View>
            <TouchableOpacity
              onPress={() => setModalVisible(true)}
              style={{ marginTop: "-130%", marginBottom: "10%" }}
            >
              <View
                style={{
                  backgroundColor: buttonCheckIn,
                  marginLeft: "19%",
                  borderRadius: 10,
                  padding: "5%",
                  alignItems: "center",
                  textAlign: "center",
                  width: "60%",
                  color: "white",
                }}
              >
                <TouchableOpacity onPress={() => setModalVisible(true)}>
                  <Text style={{ color: textModify, fontWeight: "900" }}>
                    {buttonModify}
                  </Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    </>
  );
};

export default HomeScreen;

// styles
const styles = StyleSheet.create({
  tinyCheck: {
    marginTop: "10%",
    marginLeft: "25%",
    width: "50%",
    borderRadius: 60,
    height: "40%",
  },
  tinyLogo3: {
    marginTop: "12.7%",
    marginLeft: "5.9%",
    borderRadius: 50,
    width: "23.4%",
    height: "50.3%",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "#fff8dc",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  acceptBut: {
    backgroundColor: "#5f9ea0",
    borderRadius: 10,
    padding: 10,
    marginRight: 20,
  },
  buttonClose: {
    backgroundColor: "orange",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  containerWidget: {
    flex: 1,
    justifyContent: "space-between",
    padding: 20,
  },
  top: {
    flex: 1,
    width: "100%",
    marginBottom: "10%",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  middle: {
    flex: 1,
    marginTop: "-25%",
  },
  bottom: {
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  container: {
    flex: 1,
  },
});
