import {
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import React, { useEffect, useState } from "react";
import {
  getAuth,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithCredential,
} from "firebase/auth";
import { useNavigation } from "@react-navigation/core";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import * as Google from "expo-auth-session/providers/google";
import { auth, app } from "../config";
// import { ScrollView } from "react-native-gesture-handler";

const provider = new GoogleAuthProvider();

const LoginScreen = () => {
  // State variables
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailLogin, getEmailLogin] = useState("");

  // navigation method
  const navigation = useNavigation();

  // useEffect
  // Redirect to homepage when login is successfully
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        navigation.navigate("Tabs");
      }
    });
    return unsubscribe;
  }, []);

  // Log in with Google account
  // log in with google
  const [request, response, promptAsync] = Google.useIdTokenAuthRequest({
    clientId:
      "420291539036-v7to0mrvk9mj22nv8iit92t8a75toji8.apps.googleusercontent.com",
    androidClientId:
      "420291539036-2ajpb6qvs7ld72sjlk7cl117v7vd1sbb.apps.googleusercontent.com",
  });

  React.useEffect(() => {
    if (response?.type === "success") {
      const { id_token } = response.params;
      const credential = GoogleAuthProvider.credential(id_token);
      const auth = getAuth();
      signInWithCredential(auth, credential).then(() => {
        navigation.navigate("Tabs");
      });
    }
  }, [response]);

  // login with account
  const handleLoginIn = () => {
    if (email === "" || password == "") {
      alert("Please fill in email and password");
    } else {
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          console.log("logged in with email and password: " + user.email);
          getEmailLogin(user.email);
        })
        .catch((error) => {
          alert("Email or pasword is not matched");
        });
    }
  };

  // Navigate to Phone sign in page
  const phoneHandle = () => {
    navigation.replace("Phone");
  };

  // Navigate to Phone registration page
  const handleSignUp = () => {
    navigation.navigate("SignUp");
  };

  // html
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <KeyboardAvoidingView style={styles.keyboardCss}>
        <ScrollView style={styles.scrollViewCss}>
          <View style={styles.overallContainer}>
            <View style={styles.imageCss}>
              <Image
                style={styles.companyLogo}
                source={require("../assets/logo.png")}
              ></Image>
            </View>
            <View style={styles.inputContainer}>
              <Text>Email:</Text>
              <TextInput
                placeholder="Email"
                placeholderTextColor="gray"
                onChangeText={(text) => setEmail(text)}
                style={styles.input}
              ></TextInput>
              <Text style={{ marginTop: "5%" }}>Password:</Text>
              <TextInput
                placeholder="Password"
                placeholderTextColor="gray"
                onChangeText={(text) => setPassword(text)}
                style={styles.input}
                secureTextEntry
              ></TextInput>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={handleLoginIn} style={styles.button1}>
                <Text style={styles.buttonText}>Login</Text>
              </TouchableOpacity>
            </View>
            <Text style={{ fontWeight: "bold" }}>OR</Text>
            <View style={styles.buttonGoogle}>
              <TouchableOpacity
                onPress={phoneHandle}
                style={[styles.button, styles.buttonOutline2]}
              >
                <Text style={styles.buttonText}>Login with phone</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, styles.buttonOutline2]}
                onPress={() => {
                  promptAsync();
                }}
              >
                <Text style={styles.buttonText}>
                  <FontAwesomeIcon
                    name="google"
                    color="white"
                    size={20}
                  ></FontAwesomeIcon>{" "}
                  Sign In with Google
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={handleSignUp}
                style={[styles.button, styles.buttonOutline]}
              >
                <Text style={styles.buttonOutlineText}>Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
};

export default LoginScreen;

//styles
const styles = StyleSheet.create({
  keyboardCss: {
    flex: 1,
  },
  scrollViewCss: {
    flex: 1,
    // backgroundColor: "blue",
  },
  overallContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageCss: {
    flex: 0.2,
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
  },
  inputContainer: {
    marginTop: 40,
    marginBottom: 10,
    width: "80%",
  },
  input: {
    backgroundColor: "white",
    marginTop: 5,
    height: 50,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
  },
  header: {
    fontSize: 20,
    color: "blue",
    margin: 30,
  },
  buttonContainer: {
    width: "60%",
    marginTop: 50,
  },
  button: {
    borderRadius: 10,
    backgroundColor: "green",
    padding: 15,
    borderColor: "green",
    alignItems: "center",
    textAlign: "center",
    width: "100%",
    color: "white",
  },
  buttonOutline2: {
    border: "none",
    backgroundColor: "green",
    padding: 15,
    marginTop: 5,
    borderWidth: 2,
  },
  buttonOutline: {
    border: "none",
    backgroundColor: "white",
    padding: 15,
    marginTop: 5,
    borderWidth: 2,
  },
  buttonOutlineText: {
    fontWeight: "700",
    color: "#0782F9",
    fontSize: 16,
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
  buttonGoogle: {
    width: "60%",
    marginTop: 5,
  },
  companyLogo: {
    marginTop: 30,
    width: 350,
    height: 100,
    resizeMode: "stretch",
  },
  button1: {
    marginBottom: 20,
    height: 50,
    borderRadius: 10,
    backgroundColor: "#0782F9",
    padding: 15,
    alignItems: "center",
    textAlign: "center",
    width: "100%",
    color: "white",
  },
});
