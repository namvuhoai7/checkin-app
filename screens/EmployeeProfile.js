import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
  Button,
} from "react-native";
import React, { useState, useEffect } from "react";
import { getDatabase, ref, update, get, child } from "firebase/database";
import { useNavigation } from "@react-navigation/core";
import { auth } from "../config";
import DatePicker from "react-native-neat-date-picker";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
// Optionally import the services that you want to use
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";

const EmployeeProfile = () => {
  // State variables
  const db = getDatabase();
  const userUid = auth.currentUser?.uid;
  const [name, setName] = useState("");
  const [birth, setBirth] = useState("Birthday");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [note, setNote] = useState("");
  const [email, setEmail] = useState("");
  const [Idset, setID] = useState("");
  const [company, setCompany] = useState("");
  var filter =
    /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  //navigation method
  const navigation = useNavigation();

  // UseEffect section
  // Get data from Firebase
  useEffect(() => {
    const dbRef = ref(getDatabase());
    const userUid = auth.currentUser?.uid;
    console.log(userUid);
    get(child(dbRef, `ALLXONE/` + userUid + "/Information")).then(
      (snapshot) => {
        if (snapshot.exists()) {
          const array = Object.values(snapshot.val());
          console.log(array);
          setName(array[2]);
          setAddress(array[0]);
          setBirth(array[1]);
          setNote(array[3]);
          setPhone(array[4]);
          setEmail(array[6]);
          setCompany(array[5]);
          setID(array[7]);
        } else {
          console.log("No data available");
        }
      }
    );
  }, []);

  // Update data from filled form
  function writeUserData() {
    if (
      name === "" ||
      birth === "" ||
      address === "" ||
      company === "" ||
      Idset === "" ||
      phone === "" ||
      email === ""
    ) {
      alert("Information fields are missing");
    } else if (!filter.test(email)) {
      alert("Please provide a valid email address");
    } else {
      const postData = {
        Name: name,
        Birthday: birth,
        Phone: phone,
        Address: address,
        Note: note,
        email: email,
        company: company,
        passport: Idset,
      };
      const updates = {};
      updates["/ALLXONE/" + userUid + "/Information"] = postData;
      return update(ref(db), updates)
        .then(() => {
          alert("Employee Profile added successfully!");
          navigation.navigate("Home");
        })
        .catch((error) => {
          alert(error);
        });
    }
  }
  const [showDatePicker, setShowDatePicker] = useState(false);

  const openDatePicker = () => {
    setShowDatePicker(true);
  };

  const onCancel = () => {
    // You should close the modal in here
    setShowDatePicker(false);
  };

  const onConfirm = (output) => {
    // You should close the modal in here
    setShowDatePicker(false);
    setBirth(output.dateString);
  };
  // naviaget profile page
  const handleProfile = () => {
    navigation.navigate("Home");
  };

  return (
    <>
      {/*Header*/}

      <ScrollView style={{ backgroundColor: "white" }}>
        <View style={styles.headerCss1}>
          <View>
            <TouchableOpacity style={styles.headerCss2} onPress={handleProfile}>
              <Image
                style={styles.tinyLogo3}
                source={require("../assets/front-avt.jpg")}
              ></Image>
              <Text
                style={{
                  marginTop: 70,
                  fontSize: 17,
                  color: "white",
                  fontWeight: "bold",
                  paddingLeft: "2%",
                }}
              >
                Employees profile
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <ScrollView> */}
        <View style={styles.inputContain}>
          <TextInput
            placeholder="Full name"
            placeholderTextColor="grey"
            onChangeText={(text) => setName(text)}
            style={styles.input}
            value={name}
            maxLength={20}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <View>
            <TouchableOpacity
              onPress={openDatePicker}
              style={{ marginTop: "9%" }}
            >
              <View style={{ flexDirection: "row" }}>
                <View>
                  <Text style={{ marginLeft: "14%", color: "grey" }}>
                    {birth}
                  </Text>
                </View>
                <View style={styles.container}>
                  <MaterialCommunityIcons name="calendar" size={30} />
                  <DatePicker
                    isVisible={showDatePicker}
                    mode={"single"}
                    onCancel={onCancel}
                    onConfirm={onConfirm}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <TextInput
            placeholder="Phone number"
            placeholderTextColor="grey"
            keyboardType="numeric"
            onChangeText={(text) => setPhone(text)}
            style={styles.input}
            value={phone}
            maxLength={15}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <TextInput
            placeholder="Email"
            keyboardType="email-address"
            placeholderTextColor="grey"
            textContentType="emailAddress"
            onChangeText={(text) => setEmail(text)}
            style={styles.input}
            value={email}
            maxLength={35}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <TextInput
            placeholder="Address"
            placeholderTextColor="grey"
            textContentType="addressCityAndState"
            onChangeText={(text) => setAddress(text)}
            style={styles.input}
            value={address}
            maxLength={80}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <TextInput
            placeholder="Passport/ID"
            placeholderTextColor="grey"
            onChangeText={(text) => setID(text)}
            keyboardType="numeric"
            style={styles.input}
            value={Idset}
            maxLength={18}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <TextInput
            placeholder="Company name"
            placeholderTextColor="grey"
            onChangeText={(text) => setCompany(text)}
            style={styles.input}
            value={company}
            maxLength={30}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
          <TextInput
            placeholder="Note"
            placeholderTextColor="grey"
            onChangeText={(text) => setNote(text)}
            style={styles.input}
            value={note}
            maxLength={80}
          ></TextInput>
          <View style={{ borderTopWidth: 1, borderTopColor: "black" }} />
        </View>
        {/*Button*/}
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            writeUserData();
          }}
        >
          <Text style={styles.buttonText2}>Save</Text>
        </TouchableOpacity>
      </ScrollView>
    </>
  );
};

export default EmployeeProfile;

const styles = StyleSheet.create({
  headerCss1: {
    flexDirection: "row",
    backgroundColor: "#5f9ea0",
    height: "15%",
  },
  headerCss2: {
    flex: 1,
    marginTop: 10,
    flexDirection: "row",
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    backgroundColor: "white",
    marginTop: "6%",
    width: "100%",
    paddingHorizontal: "5%",
    paddingVertical: "4%",
  },
  inputContain: {
    backgroundColor: "white",
    marginTop: "5%",
    paddingHorizontal: "5%",
    paddingVertical: "4%",
    borderRadius: 10,
  },
  button: {
    marginTop: "10%",
    marginLeft: "19%",
    marginBottom: "19%",
    borderRadius: 10,
    backgroundColor: "#5f9ea0",
    padding: "5%",
    alignItems: "center",
    textAlign: "center",
    width: "60%",
    color: "white",
  },
  buttonText2: {
    color: "white",
    fontWeight: "700",
  },

  tinyLogo3: {
    marginTop: "13.8%",
    marginLeft: "6.5%",
    borderRadius: 50,
    width: "25.4%",
    height: "50.3%",
  },
});
