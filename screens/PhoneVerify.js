import React from "react";
import {
  Text,
  View,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import {
  FirebaseRecaptchaVerifierModal,
  FirebaseRecaptchaBanner,
} from "expo-firebase-recaptcha";
import { initializeApp, getApp } from "firebase/app";
import {
  getAuth,
  PhoneAuthProvider,
  signInWithCredential,
} from "firebase/auth";
import { useNavigation } from "@react-navigation/core";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";

// Firebase references
const app = getApp();
const auth = getAuth();

// Double-check that we can run the example
if (!app?.options || Platform.OS === "web") {
  //throw new Error('This example only works on Android or iOS, and requires a valid Firebase config.');
}

export default function PhoneVerify() {
  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyCEWdo056OF2znuDynrSwXjQi9Q69m8kgI",
      authDomain: "igneous-mason-355913.firebaseapp.com",
      databaseURL: "https://igneous-mason-355913-default-rtdb.firebaseio.com",
      projectId: "igneous-mason-355913",
      storageBucket: "igneous-mason-355913.appspot.com",
      messagingSenderId: "420291539036",
      appId: "1:420291539036:web:213303452e4edb9759b699",
    });
  } else {
    firebase.app(); // if already initialized, use that one
  }
  const navigation = useNavigation();
  // Ref or state management hooks
  const recaptchaVerifier = React.useRef(null);
  const [phoneNumber, setPhoneNumber] = React.useState();
  const [verificationId, setVerificationId] = React.useState();
  const [verificationCode, setVerificationCode] = React.useState();
  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => {
        navigation.replace("Login");
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
      });
  };
  const firebaseConfig = app ? app.options : undefined;
  const [message, showMessage] = React.useState();
  const attemptInvisibleVerification = false;
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={{ padding: 20, marginTop: 50 }}>
        <FirebaseRecaptchaVerifierModal
          ref={recaptchaVerifier}
          firebaseConfig={app.options}
          // attemptInvisibleVerification
        />
        <Text style={{ marginTop: 20 }}>Enter phone number</Text>
        <TextInput
          placeholder="Phone number"
          placeholderTextColor="grey"
          style={styles.input}
          autoFocus
          autoCompleteType="tel"
          keyboardType="phone-pad"
          textContentType="telephoneNumber"
          onChangeText={(phoneNumber) => setPhoneNumber("+84" + phoneNumber)}
        />
        <TouchableOpacity
          title="Send Verification Code"
          disabled={!phoneNumber}
          onPress={async () => {
            // The FirebaseRecaptchaVerifierModal ref implements the
            // FirebaseAuthApplicationVerifier interface and can be
            // passed directly to `verifyPhoneNumber`.
            try {
              const phoneProvider = new PhoneAuthProvider(auth);
              const verificationId = await phoneProvider.verifyPhoneNumber(
                phoneNumber,
                recaptchaVerifier.current
              );
              setVerificationId(verificationId);
              showMessage({
                text: "Verification code has been sent to your phone.",
              });
            } catch (err) {
              showMessage({ text: `Error: ${err.message}`, color: "red" });
            }
          }}
          style={styles.button1}
        >
          <Text style={styles.buttonText}>Send Verification Code</Text>
        </TouchableOpacity>
        <Text style={{ marginTop: 20 }}>Enter Verification code</Text>
        <TextInput
          editable={!!verificationId}
          placeholderTextColor="grey"
          placeholder="123456"
          onChangeText={setVerificationCode}
          style={styles.input}
        />
        <TouchableOpacity
          disabled={!verificationId}
          onPress={async () => {
            try {
              const credential = PhoneAuthProvider.credential(
                verificationId,
                verificationCode
              );
              await signInWithCredential(auth, credential).then((result) => {
                console.log(result);
                console.log(result);
              });
              showMessage({ text: "Phone authentication successful 👍" });
              navigation.navigate("Tabs");
            } catch (err) {
              showMessage({ text: `Error: ${err.message}`, color: "red" });
            }
          }}
          style={styles.button1}
        >
          <Text style={styles.buttonText}>Confirm And Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={handleSignOut} style={styles.button}>
          <Text style={styles.buttonText}>Go back</Text>
        </TouchableOpacity>
        {message ? (
          <TouchableOpacity
            style={[
              StyleSheet.absoluteFill,
              { backgroundColor: 0xffffffee, justifyContent: "center" },
            ]}
            onPress={() => showMessage(undefined)}
          >
            <Text
              style={{
                color: message.color || "blue",
                fontSize: 17,
                textAlign: "center",
                margin: 20,
              }}
            >
              {message.text}
            </Text>
          </TouchableOpacity>
        ) : undefined}
        {attemptInvisibleVerification && <FirebaseRecaptchaBanner />}
      </View>
    </TouchableWithoutFeedback>
  );
}
const styles = StyleSheet.create({
  button: {
    marginTop: 30,
    borderRadius: 10,
    backgroundColor: "orange",
    padding: 15,
    alignItems: "center",
    textAlign: "center",
    width: "100%",
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
  input: {
    backgroundColor: "white",
    marginTop: 10,
    width: 375,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
  },
  button1: {
    marginTop: 30,
    borderRadius: 10,
    backgroundColor: "green",
    padding: 15,
    alignItems: "center",
    textAlign: "center",
    width: "100%",
  },
});
