import {
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
} from "react-native";
import React, { useEffect, useState } from "react";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { getDatabase, ref, set } from "firebase/database";
import { useNavigation } from "@react-navigation/core";
import { auth, app } from "../config";

const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordC, setPasswordC] = useState("");
  const [emailO, getEmailO] = useState("");

  const navigation = useNavigation();

  const handleSignUp = () => {
    if (email === "" || password == "" || passwordC == "") {
      alert("All inputs much be filled");
    } else {
      if (passwordC === password && password.length > 6) {
        const authenticate = getAuth();
        createUserWithEmailAndPassword(authenticate, email, password)
          .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            getEmailO(email);
            alert("Sign up successfully");
            const db = getDatabase();
            set(ref(db, "ALLXONE/" + user.uid), {
              Email: user.email,
            });
            navigation.navigate("Tabs");
          })
          .catch((error) => {
            alert("Email is already in use or email type is invalid");
          });
      } else {
        alert(
          "Confirm password is incorrect or password must be at least 6 characters"
        );
      }
    }
  };

  const handleGoBack = () => {
    navigation.navigate("Login");
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <ScrollView>
          <View style={styles.viewCss}>
            <Text
              style={{
                fontSize: 35,
                color: "#f50057",
                fontWeight: "bold",
                marginBottom: 60,
              }}
            >
              Sign up new account
            </Text>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Your Email"
                placeholderTextColor="grey"
                onChangeText={(text) => setEmail(text)}
                style={styles.input}
              ></TextInput>
              <TextInput
                placeholder="Your Password"
                placeholderTextColor="gray"
                onChangeText={(text) => setPassword(text)}
                style={styles.input}
                secureTextEntry
              ></TextInput>
              <TextInput
                placeholder="Confirm password"
                placeholderTextColor="gray"
                onChangeText={(text) => setPasswordC(text)}
                style={styles.input}
                secureTextEntry
              ></TextInput>
            </View>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                onPress={handleGoBack}
                style={[styles.button, styles.buttonOutline2]}
              >
                <Text style={styles.buttonOutlineText}>Go back</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleSignUp}
                style={[styles.button, styles.buttonOutline]}
              >
                <Text style={styles.buttonOutlineText}>Sign Up</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: "30%",
  },
  inputContainer: {
    width: "90%",
    height: 200,
  },
  input: {
    backgroundColor: "white",
    marginTop: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    height: 50,
  },
  header: {
    fontSize: 20,
    color: "blue",
    margin: 30,
  },
  buttonContainer: {
    width: "40%",
    marginTop: 40,
  },
  button: {
    borderRadius: 10,
    backgroundColor: "#0782F9",
    padding: 15,
    alignItems: "center",
    textAlign: "center",
    width: "40%",
    marginLeft: 10,
    marginRight: 10,
    color: "white",
  },
  buttonOutline2: {
    backgroundColor: "orange",
    borderColor: "orange",
    padding: 15,
    marginTop: 5,
    borderWidth: 2,
  },
  buttonOutline: {
    backgroundColor: "#0782F9",
    borderColor: "#0782F9",
    padding: 15,
    marginTop: 5,
    borderWidth: 2,
  },
  buttonOutlineText: {
    fontWeight: "700",
    color: "white",
    fontSize: 16,
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
  buttonGoogle: {
    width: "60%",
    // justifyContent: "center",
    // alignContent: "center",
    marginTop: 5,
  },
  tinyLogo: {
    width: 100,
    height: 100,
    resizeMode: "stretch",
  },
  viewCss: {
    flex: 1,
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});
